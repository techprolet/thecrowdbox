'use strict'

    //~ server.js
    //~ Part of "The Crowd Box" project
    //~ Copyright 2017 Pavlos Iliopoulos, techprolet.com
    //~ This program is free software: you can redistribute it and/or modify
    //~ it under the terms of the GNU General Public License as published by
    //~ the Free Software Foundation, either version 3 of the License, or
    //~ (at your option) any later version.
 
    //~ This program is distributed in the hope that it will be useful,
    //~ but WITHOUT ANY WARRANTY; without even the implied warranty of
    //~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //~ GNU General Public License for more details.
 
    //~ You should have received a copy of the GNU General Public License
    //~ along with this program.  If not, see <http://www.gnu.org/licenses/>.

var express = require('express');
var pcm = require('pcm-util')
const play = require('audio-play');
var WebSocketServer = require('ws').Server;
var AudioContext = require('web-audio-api').AudioContext;
var app = express();

var device = true; //set this to false, when testing on the pc

//the following is used in off-edison development
var keypress = require('keypress');

if (device) {
	var five = require("johnny-five");
	var Edison = require("edison-io");
	var board = new five.Board({
		io: new Edison(Edison.Boards.Xadow)
	});
}

var buttonPressed = false;
var lastButtonState = false;
var connections = [];
var activeConnection = null;
var led = null;

//use this when directly writing to stdout:
//aplay for the pcm version (index.html)
//mpg123 for the mp3 version (index_lame.html)
//node server.js | aplay -f S16_LE -c1 -r44100
//for 48khz:
//node server.js | aplay -f S16_LE -c1 -r48000
//node server.js | mpg123 -




function updateLedState() {
	if (led != null) {
		if (activeConnection != null) {
			led.blink();
		} else {
			led. stop();
			
			if (connections.length > 0) {
				led.on();
			} else {
				led.off();
			}
		}
	}
}



app.use(express.static(__dirname + '/public'));
app.listen(8080);
var wss = new WebSocketServer({port: 3000, path: '/data', ip: '0.0.0.0'});

wss.on('connection', function(ws) {

	connections.push(ws);

	ws.on('message', function(message) {
		var buff = new Buffer(message);
		process.stdout.write(buff);
	});

	ws.on('close', function(reasonCode, description) {
		var i = connections.indexOf(ws);
		connections.splice(i, 1);
		
		if (activeConnection == ws) {
			activeConnection = null;			
		}
		
		updateLedState();
		//~ console.log("one client was disconnected");
	});
	
	updateLedState();
});

 
if (device) {
	board.on("ready", function() {
	  led = new five.Led(13);
	});

	board.digitalRead(8, function(data) {
	  if (data != lastButtonState) {
		lastButtonState = data;
		console.log(data);
		if (lastButtonState) {
			//console.log("Button pressed!");	
			toggleStream();
		}
	  }
	});
} else {
	// make `process.stdin` begin emitting "keypress" events 
	keypress(process.stdin);
	 
	// listen for the "keypress" event 
	process.stdin.on('keypress', function (ch, key) {
	  //console.log('got "keypress"', key);
	  if (key && key.ctrl && key.name == 'c') {
	    process.exit(0);
	  }
	  toggleStream();
	});
	 
	process.stdin.setRawMode(true);
	process.stdin.resume();
}


function toggleStream() {
	if (connections.length > 0) {
		if (activeConnection != null) {
			activeConnection. send ("stop");
			activeConnection = null;
			//~ console.log ("Streaming stopped. Available connections:" + connections.length);
		} else {
			var connectionIndex = Math.floor(Math.random() * connections.length);			
			activeConnection = connections[connectionIndex];
			activeConnection.send("start");
		}
	}
	
	updateLedState();
}