This code is part of "The Crowd Box" project

Where no other credit and/or copyright notice is given:

Copyright 2017 Pavlos Iliopoulos, techprolet.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

lamejs is a project found here: https://github.com/zhuker/lamejs
It is based on the LAME project, which is released under the LGPL License.
The LAME project's home is http://lame.sourceforge.net/


A repository where you can find an mpg123 build for the Yocto distribution can be found here:
http://alextgalileo.altervista.org/edison-package-repo-configuration-instructions.html

This software was implemented for the Intel Edison board. You should be able, however,
to run this code with minor changes on any audio-capable linux machine.
To run on a PC, set the variable >>device<< (in server.js) to false.

Webclients connected to the node.js server should be fully HTML5 Audio standard compatible.
Tested and working with Firefox.




